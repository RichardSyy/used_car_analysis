from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
import re
import pandas as pd
import json


def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the
    text content, otherwise return None.
    """
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error("Error during requests to {0} : {1}".format(url, str(e)))
        return None


def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers["Content-Type"].lower()
    return (
        resp.status_code == 200
        and content_type is not None
        and content_type.find("html") > -1
    )


def log_error(e):
    """
    It is always a good idea to log errors. 
    This function just prints them, but you can
    make it do anything.
    """
    print(e)


def get_listings_from_carsdotcom(zipcode, radius, num_page, per_page):
    vehicle_data_list = []
    for i in range(min(num_page, 50)):
        print(f"Scraping data for page {i+1}...")
        url = f"https://www.cars.com/for-sale/searchresults.action/?dealerType=localOnly&page={i+1}&perPage={per_page}&rd={radius}&searchSource=GN_BREADCRUMB&sort=relevance&stkTypId=28881&zc={zipcode}"
        raw = simple_get(url)

        parsed = BeautifulSoup(raw, "html.parser")

        raw_selected_data = parsed.select("script")[
            1
        ].text  # select the second 'script' item

        raw_digitalData = (
            raw_selected_data.split("};")[1]
            .strip()
            .replace("CARS.digitalData = ", "")
        )
        raw_digitalData += "}"

        digitalData_dict = json.loads(raw_digitalData)["page"]
        page_vehicle_data = pd.DataFrame(digitalData_dict["vehicle"])[
            [
                "make",
                "model",
                "year",
                "mileage",
                "trim",
                "price",
                "privateSeller",
                "stockType",
            ]
        ]
        vehicle_data_list.append(page_vehicle_data)
    vehicle_data = pd.concat(vehicle_data_list, ignore_index=True)
    return vehicle_data
