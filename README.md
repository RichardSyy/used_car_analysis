# Data Scraping and Analysis Project  
# Initiated at July 6th, 2019

## Goal:
* Scrape the used-cars listing websites (cars.com, carmax.com, carfax.com, autotrader.com) for listing data.  
* Descriptive statistics of the listings in the Chicagoland.  
* Understand the resale value of various makes and models.  

Scraping framework in Python inspired by `Colin OKeefe` from https://realpython.com/python-web-scraping-practical-introduction/.